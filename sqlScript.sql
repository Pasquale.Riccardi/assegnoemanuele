create schema springrestsecurity;
use springrestsecurity;

create table utenti (
id int not null auto_increment primary key,
username varchar(35) not null,
pwd varchar(35) not null
);

create table roles (
id int not null auto_increment primary key,
ruolo varchar(35),
iduser int,
foreign key (iduser) references utenti(id)
);
