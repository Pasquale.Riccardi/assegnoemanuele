package com.esercizi.SpringMVC.utilities;

import com.esercizi.SpringMVC.domain.Detail;
import com.esercizi.SpringMVC.domain.Product;
import com.esercizi.SpringMVC.domain.Store;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class SetupClass {

    private List<Product> prodotti=new ArrayList<>();
    private List<Detail> dettagli=new ArrayList<>();
    private List<Store> stores=new ArrayList<>();
    Product test1=new Product(1l,"test1");
    Product test2=new Product(2l,"test2");
    Product test3=new Product(3l,"test3");
    Detail test1d=new Detail(1l,"test1");
    Detail test2d=new Detail(2l,"test2");
    Detail test3d=new Detail(3l,"test3");
    Store test1s=new Store(1l,"test1");
    Store test2s=new Store(2l,"test2");
    Store test3s=new Store(3l,"test3");


    @PostConstruct
    public void setup() {
        prodotti.addAll(Arrays.asList(test1,test2,test3));
        dettagli.addAll(Arrays.asList(test1d,test2d,test3d));
        stores.addAll(Arrays.asList(test1s,test2s,test3s));
    }

    public List<Product> getProdotti() {
        return prodotti;
    }

    public List<Detail> getDettagli() {
        return dettagli;
    }

    public List<Store> getStores() {
        return stores;
    }
}
