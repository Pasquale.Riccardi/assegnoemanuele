package com.esercizi.SpringMVC.utilities;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Component
public class LogoutUtil {

    private List<String> listaNera;

    public LogoutUtil() {
        this.listaNera = new ArrayList<>();
    }

    public void blackLister(String token) {
        listaNera.add(token);
        TimerTask rimuovi=new TimerTask() {
            @Override
            public void run() {
                listaNera.remove(token);
            }
        };
        Timer timer=new Timer();
        long tempo=900000l;
        timer.schedule(rimuovi,tempo);
    }

    public List<String> getListaNera() {
        return listaNera;
    }

    public void setListaNera(List<String> listaNera) {
        this.listaNera = listaNera;
    }
}
