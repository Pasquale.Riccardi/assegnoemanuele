package com.esercizi.SpringMVC.securityConfig;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.esercizi.SpringMVC.utilities.LogoutUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;

public class CustomAuthorizationFilter extends OncePerRequestFilter {

    private LogoutUtil util;

    public CustomAuthorizationFilter(LogoutUtil util) {
        this.util = util;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if(request.getServletPath().equals("/authn")) {
            filterChain.doFilter(request,response);
        }
        else {
            String authHeader=request.getHeader(AUTHORIZATION);
            if (authHeader!=null && !util.getListaNera().contains(authHeader)) {
                    Algorithm algorithm = Algorithm.HMAC256("segreto".getBytes());
                    JWTVerifier verifier = JWT.require(algorithm).build();
                    DecodedJWT decodedToken = verifier.verify(authHeader);
                    String user=decodedToken.getSubject();
                    String[] ruoli=decodedToken.getClaim("roles").asArray(String.class);
                    List<SimpleGrantedAuthority> authorities=new ArrayList<>();
                    Arrays.stream(ruoli).forEach(x-> {
                        authorities.add(new SimpleGrantedAuthority(x));
                    });
                    UsernamePasswordAuthenticationToken authenticationToken=
                            new UsernamePasswordAuthenticationToken(user,null,authorities);
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                    filterChain.doFilter(request,response);
            }
            else {
                filterChain.doFilter(request,response);
            }
        }
    }
}
