package com.esercizi.SpringMVC.securityConfig;


import com.esercizi.SpringMVC.utilities.LogoutUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService detailsService;
    private LogoutUtil util;

    @Autowired
    public WebSecurityConfig(UserDetailsService detailsService,LogoutUtil util) {
        this.detailsService = detailsService;
        this.util=util;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(detailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomAuthenticationFilter filter=new CustomAuthenticationFilter(authenticationManagerBean());
        filter.setFilterProcessesUrl("/authn");
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.addFilter(filter);
        http.addFilterBefore(new CustomAuthorizationFilter(util), UsernamePasswordAuthenticationFilter.class);
        DefaultWebSecurityExpressionHandler handler=new DefaultWebSecurityExpressionHandler();
        handler.setRoleHierarchy(gerarchia());

        http.authorizeRequests().antMatchers("/product/").hasRole("ICM");
        http.authorizeRequests().antMatchers("/store/").hasRole("MKT");
        http.authorizeRequests().antMatchers("/detail/").hasRole("CP");


    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public RoleHierarchy gerarchia() {
        RoleHierarchyImpl laGerarchia=new RoleHierarchyImpl();
        String hierarchy="ROLE_ADMIN > ROLE_ICM \n ROLE_ADMIN > ROLE_MKT \n ROLE_ADMIN > ROLE_CP";
        laGerarchia.setHierarchy(hierarchy);
        return laGerarchia;
    }

}
