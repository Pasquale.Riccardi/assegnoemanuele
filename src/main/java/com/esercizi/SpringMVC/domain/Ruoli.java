package com.esercizi.SpringMVC.domain;


import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Ruoli {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name ="ruolo")
    private String ruolo;
    @ManyToOne
    @JoinColumn(name = "iduser",referencedColumnName = "id")
    private Utenti utente;

    public Ruoli() {
    }

    public Ruoli(String ruolo, Utenti utente) {
        this.ruolo = ruolo;
        this.utente = utente;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public Utenti getUtente() {
        return utente;
    }

    public void setUtente(Utenti utente) {
        this.utente = utente;
    }

    @Override
    public String toString() {
        return "Ruoli{" +
                "id=" + id +
                ", ruolo='" + ruolo + '\'' +
                '}';
    }
}
