package com.esercizi.SpringMVC.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "utenti")
public class Utenti {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "username")
    private String username;
    @Column(name = "pwd")
    private String password;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "utente",orphanRemoval = true)
    private Set<Ruoli> ruoli=new HashSet<>();

    public Utenti() {
    }

    public Utenti(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Ruoli> getRuoli() {
        return ruoli;
    }

    public void setRuoli(Set<Ruoli> ruoli) {
        this.ruoli = ruoli;
    }

    @Override
    public String toString() {
        return "Utenti{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", ruoli=" + ruoli +
                '}';
    }
}
