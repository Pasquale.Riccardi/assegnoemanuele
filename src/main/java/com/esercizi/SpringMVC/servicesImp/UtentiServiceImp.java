package com.esercizi.SpringMVC.servicesImp;


import com.esercizi.SpringMVC.domain.Ruoli;
import com.esercizi.SpringMVC.domain.Utenti;
import com.esercizi.SpringMVC.repos.UtentiRepo;
import com.esercizi.SpringMVC.services.UtentiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UtentiServiceImp implements UtentiService, UserDetailsService {

    private final UtentiRepo repo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Utenti> utente=Optional.of(repo.findByUsername(username));
        if(utente.isEmpty()) {
            throw new UsernameNotFoundException("username non presente");
        }
        else {
            System.out.println("utente trovato nel db!");
        }
        List<SimpleGrantedAuthority> auts=new ArrayList<>();
        utente.get().getRuoli().stream()
                .forEach(x-> auts.add(new SimpleGrantedAuthority(x.getRuolo())));
        return new User(utente.get().getUsername(),utente.get().getPassword(),auts);
    }

    @Autowired
    public UtentiServiceImp(UtentiRepo repo) {
        this.repo = repo;
    }


    @Override
    public List<Utenti> findAll() {
        return repo.findAll();
    }

    @Override
    public Utenti findOne(String username) {
        return repo.findByUsername(username);
    }

    @Override
    public List<Ruoli> findAll(String username) {
        Utenti target=repo.findByUsername(username);
        return target.getRuoli().stream().collect(Collectors.toList());
    }


}
