package com.esercizi.SpringMVC.servicesImp;

import com.esercizi.SpringMVC.domain.Detail;
import com.esercizi.SpringMVC.domain.Store;
import com.esercizi.SpringMVC.services.GenericService;
import com.esercizi.SpringMVC.utilities.SetupClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class StoreServiceImp implements GenericService<Store> {

    private SetupClass setup;
    List<Store> stores;

    @Autowired
    public StoreServiceImp(SetupClass setup) {
        this.setup = setup;
    }

    @PostConstruct
    public void injectingLove() {
        stores =setup.getStores();
    }

    @Override
    public List<Store> findAll() {
        return stores;
    }

    @Override
    public Store findOne(long id) {
        return stores.stream().filter(x->x.getId()==id).findFirst().orElseGet(null);
    }

    @Override
    public void create(Store store) {
        stores.add(store);
    }

    @Override
    public void modify(Store store) {
        stores.replaceAll(x->{
            if (x.getId() == store.getId()) {
                return store;
            }
            return x;
        });
    }

    @Override
    public void delete(long id) {
        stores.replaceAll(x->{
            if (x.getId()==id) {
                return null;
            }
            return x;
        });
        stores.remove(null);
    }
}
