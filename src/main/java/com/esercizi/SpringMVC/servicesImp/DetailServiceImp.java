package com.esercizi.SpringMVC.servicesImp;

import com.esercizi.SpringMVC.domain.Detail;
import com.esercizi.SpringMVC.services.GenericService;
import com.esercizi.SpringMVC.utilities.SetupClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class DetailServiceImp implements GenericService<Detail> {

    private SetupClass setup;
    List<Detail> dettagli;

    @Autowired
    public DetailServiceImp(SetupClass setup) {
        this.setup = setup;
    }

    @PostConstruct
    public void injectingLove() {
        dettagli =setup.getDettagli();
    }

    @Override
    public List<Detail> findAll() {
        return dettagli;
    }

    @Override
    public Detail findOne(long id) {
        return dettagli.stream().filter(x->x.getId()==id).findFirst().orElseGet(null);
    }

    @Override
    public void create(Detail dettaglio) {
        dettagli.add(dettaglio);
    }

    @Override
    public void modify(Detail dettaglio) {
        dettagli.replaceAll(x->{
            if (x.getId() == dettaglio.getId()) {
                return dettaglio;
            }
            return x;
        });
    }

    @Override
    public void delete(long id) {
        dettagli.replaceAll(x->{
            if (x.getId()==id) {
                return null;
            }
            return x;
        });
        dettagli.remove(null);
    }
}
