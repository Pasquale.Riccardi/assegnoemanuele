package com.esercizi.SpringMVC.servicesImp;

import com.esercizi.SpringMVC.domain.Product;
import com.esercizi.SpringMVC.services.GenericService;
import com.esercizi.SpringMVC.utilities.SetupClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;


@Service
public class ProductServiceImp implements GenericService<Product> {

    private SetupClass setup;
    List<Product> prodotti;

    @Autowired
    public ProductServiceImp(SetupClass setup) {
        this.setup = setup;
    }

    @PostConstruct
    public void injectingLove() {
        prodotti=setup.getProdotti();
    }

    @Override
    public List<Product> findAll() {
        return prodotti;
    }

    @Override
    public Product findOne(long id) {
        return prodotti.stream().filter(x->x.getId()==id).findFirst().orElseGet(null);
    }

    @Override
    public void create(Product product) {
        prodotti.add(product);
    }

    @Override
    public void modify(Product product) {
        prodotti.replaceAll(x->{
            if (x.getId() == product.getId()) {
                return product;
            }
            return x;
        });
    }

    @Override
    public void delete(long id) {
        prodotti.replaceAll(x->{
            if (x.getId()==id) {
                return null;
            }
            return x;
        });
        prodotti.remove(null);
    }
}
