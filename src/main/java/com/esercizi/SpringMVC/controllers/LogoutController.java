package com.esercizi.SpringMVC.controllers;

import com.esercizi.SpringMVC.utilities.LogoutUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogoutController {

    @Autowired
    private LogoutUtil util;

    @RequestMapping("/logout")
    public void logout(@RequestHeader("Authorization") String token) {
        util.blackLister(token);
    }
}
