package com.esercizi.SpringMVC.controllers;


import com.esercizi.SpringMVC.domain.Detail;
import com.esercizi.SpringMVC.domain.Product;
import com.esercizi.SpringMVC.servicesImp.DetailServiceImp;
import com.esercizi.SpringMVC.servicesImp.ProductServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@RestController
@RequestMapping("detail")
public class DetailController {

    private final DetailServiceImp service;

    @Autowired
    public DetailController(DetailServiceImp service) {
        this.service = service;
    }

    @GetMapping("/")
    public List<Detail> tutti() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public Detail trovaUno(@PathVariable long id) {
        return service.findOne(id);
    }

    @PostMapping("/")
    public void salva(@RequestBody Detail dettaglio) {
        service.create(dettaglio);
    }

    @PutMapping("/")
    public void modifica(@RequestBody Detail dettaglio) {
        service.modify(dettaglio);
    }

    @DeleteMapping("/{id}")
    public void cancella(@PathVariable long id) {
        service.delete(id);
    }

}
