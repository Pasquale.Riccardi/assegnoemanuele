package com.esercizi.SpringMVC.controllers;


import com.esercizi.SpringMVC.domain.Detail;
import com.esercizi.SpringMVC.domain.Store;
import com.esercizi.SpringMVC.servicesImp.DetailServiceImp;
import com.esercizi.SpringMVC.servicesImp.StoreServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@RestController
@RequestMapping("store")
public class StoreController {

    private final StoreServiceImp service;

    @Autowired
    public StoreController(StoreServiceImp service) {
        this.service = service;
    }

    @GetMapping("/")
    public List<Store> tutti() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public Store trovaUno(@PathVariable long id) {
        return service.findOne(id);
    }

    @PostMapping("/")
    public void salva(@RequestBody Store negozio) {
        service.create(negozio);
    }

    @PutMapping("/")
    public void modifica(@RequestBody Store negozio) {
        service.modify(negozio);
    }

    @DeleteMapping("/{id}")
    public void cancella(@PathVariable long id) {
        service.delete(id);
    }


}
