package com.esercizi.SpringMVC.controllers;


import com.esercizi.SpringMVC.domain.Product;
import com.esercizi.SpringMVC.servicesImp.ProductServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {

    private final ProductServiceImp service;

    @Autowired
    public ProductController(ProductServiceImp service) {
        this.service = service;
    }

    @GetMapping("/")
    public List<Product> tutti() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public Product trovaUno(@PathVariable long id) {
        return service.findOne(id);
    }

    @PostMapping("/")
    public void salva(@RequestBody Product prodotto) {
        service.create(prodotto);
    }

    @PutMapping("/")
    public void modifica(@RequestBody Product product) {
        service.modify(product);
    }

    @DeleteMapping("/{id}")
    public void cancella(@PathVariable long id) {
        service.delete(id);
    }
}
