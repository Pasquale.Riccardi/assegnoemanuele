package com.esercizi.SpringMVC.execptionHandling;

public class customEx {
    private int codice;
    private String messaggio;
    private String orario;

    public customEx(int codice, String messaggio, String orario) {
        this.codice = codice;
        this.messaggio = messaggio;
        this.orario = orario;
    }

    public int getCodice() {
        return codice;
    }

    public void setCodice(int codice) {
        this.codice = codice;
    }

    public String getMessaggio() {
        return messaggio;
    }

    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

    public String getOrario() {
        return orario;
    }

    public void setOrario(String orario) {
        this.orario = orario;
    }
}
