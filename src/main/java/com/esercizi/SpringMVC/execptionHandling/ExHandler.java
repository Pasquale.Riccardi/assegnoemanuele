package com.esercizi.SpringMVC.execptionHandling;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.Clock;

@ControllerAdvice
public class ExHandler {

    @ExceptionHandler
    ResponseEntity<customEx> handler(Exception e) {
        Clock orario=Clock.systemDefaultZone();
        customEx ex=new customEx(HttpStatus.BAD_REQUEST.value(),e.getMessage(),orario.instant().toString());
        return new ResponseEntity<>(ex,HttpStatus.BAD_REQUEST);
    }

}
