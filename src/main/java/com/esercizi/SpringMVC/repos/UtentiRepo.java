package com.esercizi.SpringMVC.repos;

import com.esercizi.SpringMVC.domain.Utenti;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtentiRepo extends JpaRepository<Utenti,Long> {
    Utenti findByUsername(String username);
}
