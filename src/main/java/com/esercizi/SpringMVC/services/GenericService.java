package com.esercizi.SpringMVC.services;



import java.util.List;

public interface GenericService<T> {
    List<T> findAll();
    T findOne(long id);
    void create(T detail);
    void modify(T detail);
    void delete(long id);
}
