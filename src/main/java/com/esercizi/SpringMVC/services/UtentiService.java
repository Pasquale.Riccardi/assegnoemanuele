package com.esercizi.SpringMVC.services;

import com.esercizi.SpringMVC.domain.Ruoli;
import com.esercizi.SpringMVC.domain.Utenti;

import java.util.List;

public interface UtentiService {
    List<Utenti> findAll();
    Utenti findOne(String username);
    List<Ruoli> findAll(String username);
}
